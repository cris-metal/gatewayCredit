(function() {
    'use strict';

    angular
        .module('gatewayApp')
        .factory('CustomerSearch', CustomerSearch);

    CustomerSearch.$inject = ['$resource'];

    function CustomerSearch($resource) {
        return $resource('proposals/' + 'api/findCustomers/:cpf',{cpf:'@cpf'});
    }

})();
